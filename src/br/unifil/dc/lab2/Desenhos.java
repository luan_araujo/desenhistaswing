package br.unifil.dc.lab2;

import javax.swing.*;
import java.awt.*;
import java.util.Scanner;


/**
 * Write a description of class Desenhos here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Desenhos
{
    public static void desenhoLivre(Graphics2D pincel, Dimension dim) {
        gabinete(pincel);
    }

    public static void desenharAsterisco(Graphics2D g2d) {
        int[] superiorEsquerdo = {50, 50};
        int[] dimensoesAsterisco = {500, 500};
        g2d.drawLine(superiorEsquerdo[0],superiorEsquerdo[1], dimensoesAsterisco[0],dimensoesAsterisco[1]);
        g2d.drawLine(superiorEsquerdo[0],dimensoesAsterisco[0],dimensoesAsterisco[1],superiorEsquerdo[1]);
        g2d.drawLine(superiorEsquerdo[0],dimensoesAsterisco[0]/2+25,dimensoesAsterisco[0],dimensoesAsterisco[1]/2+25);
        g2d.drawLine(dimensoesAsterisco[0]/2+25,superiorEsquerdo[0],dimensoesAsterisco[1]/2+25,dimensoesAsterisco[1]);
    }

    public static void smile(Graphics2D pincel) {
        BasicStroke largura = new BasicStroke(6);
        pincel.setStroke(largura);
        pincel.setColor(Color.BLACK);
        pincel.drawOval(50, 50, 160, 160);
        pincel.setColor(Color.YELLOW);
        pincel.fillOval(53,53,154,154);
        pincel.setColor(Color.BLACK);
        pincel.drawLine(75,160,185,160);
        pincel.fillOval(93,88,25,25);
        pincel.fillOval(143,88,25,25);
    }

    public static void gabinete(Graphics2D pincel) {
        pincel.setColor(Color.BLACK);
        pincel.drawRect(50,50,150,300);//gabinete

        pincel.drawRect(65, 60, 120, 40);//baia 1
        pincel.drawRect(76, 70,97, 17);//compartimento baia 1
        pincel.drawRect(150, 90, 17,5);//botão baia 1

        pincel.drawRect(65, 100, 120, 40);//baia 2
        pincel.drawRect(76, 110,97, 17);//compartimento baia 2
        pincel.drawRect(150, 130, 17,5);//botão baia 2

        pincel.drawRect(65,140, 120,40);//baia 3
        pincel.drawRect(76, 150,97, 17);//compartimento baia 3
        pincel.drawRect(150, 170, 17,5);//botão baia 3

        pincel.drawRect(80, 190, 85, 20);//disquete 1
        pincel.drawRect(85, 195, 75, 8);//compartimento disquete 1
        pincel.drawRect(145, 205, 10, 2);//botão 1

        pincel.drawRect(80, 210, 85, 20);//disquete 1
        pincel.drawRect(85, 215, 75, 8);//compartimento disquete 1
        pincel.drawRect(145, 225, 10, 2);//botão 1

        pincel.drawRect(80, 230, 85, 20);

        //////////////////USB 1//////////////////////////
        pincel.drawRect(85, 235, 12, 8);
        pincel.fillRect(87, 237, 9, 3);

        //////////////////USB 2//////////////////////////
        pincel.drawRect(148, 235, 12, 8);
        pincel.fillRect(150, 237, 9, 3);

        /////////////////conector de microfone e fone de ouvido/////////////////////
        pincel.drawOval(115, 240, 4,4);
        pincel.drawOval(126, 240, 4,4);

        ///////////////////botão power///////////////////////
        BasicStroke largura = new BasicStroke(2);
        pincel.setStroke(largura);
        pincel.drawOval(113,270,20,20);
        pincel.clearRect(120, 268,6,10);
        BasicStroke largura2 = new BasicStroke(2);
        pincel.setStroke(largura2);
        pincel.drawLine(123, 278, 123,267);

        ////////////////////////Marca do computador////////////////////
        Font myFont = new Font(Font.SERIF, Font.BOLD | Font.ITALIC, 16);
        pincel.setFont(myFont);
        pincel.drawString("AlunoTEC", 90, 325);
    }

    public static void bandeira(Graphics2D pincel, Dimension dim){
        final int[] superiorEsquerdoAsterisco = { (int) (.1f * dim.width), (int) (.1f * dim.height) }; // x,y
        final int[] dimensoesAsterisco = { (int) (.5f * dim.width), (int) (.5f * dim.height) }; // w,h
        final int X = 0, Y = 1, W = 0, H = 1;

        Color myColor = new Color(35,142,35);
        pincel.setColor(myColor);
        pincel.fillRect(superiorEsquerdoAsterisco[X], superiorEsquerdoAsterisco[Y],
                superiorEsquerdoAsterisco[X] + dimensoesAsterisco[W],
                superiorEsquerdoAsterisco[Y] + dimensoesAsterisco[H]-50);
        pincel.setColor(Color.YELLOW);
        int[] arr1 = {300, 150, 300, 450};
        int[] arr2 = {100, 200, 300, 200};
        Polygon losango = new Polygon(arr1,arr2, 4);
        pincel.fillPolygon(losango);
    }
}